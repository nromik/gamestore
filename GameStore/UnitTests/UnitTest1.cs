﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Domain.Abstract;
using Domain.Entities;
using WebUI.Controllers;
using WebUI.HtmlHelpers;
using WebUI.Models;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Can_Paginate()
        {
            // Организация (arrange)
            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games).Returns(new List<Game>()
            {
                new Game {GameId = 1, Name = "Игра1"},
                new Game {GameId = 2, Name = "Игра2"},
                new Game {GameId = 3, Name = "Игра3"},
                new Game {GameId = 4, Name = "Игра4"},
                new Game {GameId = 5, Name = "Игра5"}
            });
            GameController controller = new GameController(mock.Object)
            {
                pageSize = 3
            };
            int page = 2;

            // Действие (act)
            var result = (GamesListViewModel) controller.List(null, page).Model;


            // Утверждение (assert)
            List<Game> games = result.Games.ToList();
            Assert.IsTrue(games.Count == page);
            Assert.AreEqual(games[0].Name, "Игра4");
            Assert.AreEqual(games[1].Name, "Игра5");
        }


        //<nav aria-label="Page navigation"><ul class="pagination"><li class="disabled"><a aria-label="Previous"><span aria-hidden="true">«</span></a></li><li class="active"><a href = "/Page1" > 1 < span class="sr-only">(current)</span></a></li><li><a href = "/Page2" > 2 </ a ></ li >< li >< a href="/Page3">3</a></li><li class=""><a aria-label="Next" href="/Page2"><span aria-hidden="true">»</span></a></li></ul></nav>

        [TestMethod]
        public void Can_Generate_Page_Links()
        {

            // Организация - определение вспомогательного метода HTML - это необходимо
            // для применения расширяющего метода
            HtmlHelper myHelper = null;

            // Организация - создание объекта PagingInfo
            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            // Организация - настройка делегата с помощью лямбда-выражения
            Func<int, string> pageUrlDelegate = i => "Page" + i;

            // Действие
            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);

            // Утверждение

            Assert.AreEqual("<nav aria-label=\"Page navigation\">" +
                            "<ul class=\"pagination\">" +
                            "<li><a aria-label=\"Previous\" href=\"Page1\"><span aria-hidden=\"true\">&laquo;</span></a></li>" +
                            "<li><a href=\"Page1\">1</a></li>" +
                            "<li class=\"active\"><a href=\"Page2\">2<span class=\"sr-only\">(current)</span></a></li>" +
                            "<li><a href=\"Page3\">3</a></li>" +
                            "<li><a aria-label=\"Next\" href=\"Page3\"><span aria-hidden=\"true\">&raquo;</span></a></li>" +
                            "</ul></nav>", result.ToString());
        }

        [TestMethod]
        public void Can_Generate_Page_Links_Previous()
        {

            // Организация - определение вспомогательного метода HTML - это необходимо
            // для применения расширяющего метода
            HtmlHelper myHelper = null;

            // Организация - создание объекта PagingInfo
            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 1,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            // Организация - настройка делегата с помощью лямбда-выражения
            Func<int, string> pageUrlDelegate = i => "Page" + i;

            // Действие
            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);

            // Утверждение

            Assert.AreEqual("<nav aria-label=\"Page navigation\">" +
                            "<ul class=\"pagination\">" +
                            "<li class=\"disabled\"><a aria-label=\"Previous\"><span aria-hidden=\"true\">&laquo;</span></a></li>" +
                            "<li class=\"active\"><a href=\"Page1\">1<span class=\"sr-only\">(current)</span></a></li>" +
                            "<li><a href=\"Page2\">2</a></li><li><a href=\"Page3\">3</a></li>" +
                            "<li><a aria-label=\"Next\" href=\"Page2\"><span aria-hidden=\"true\">&raquo;</span></a></li>" +
                            "</ul></nav>", result.ToString());
        }

        [TestMethod]
        public void Can_Generate_Page_Links_Next()
        {

            // Организация - определение вспомогательного метода HTML - это необходимо
            // для применения расширяющего метода
            HtmlHelper myHelper = null;

            // Организация - создание объекта PagingInfo
            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 3,
                TotalItems = 28,
                ItemsPerPage = 10
            };

            // Организация - настройка делегата с помощью лямбда-выражения
            Func<int, string> pageUrlDelegate = i => "Page" + i;

            // Действие
            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);

            // Утверждение

            Assert.AreEqual("<nav aria-label=\"Page navigation\">" +
                            "<ul class=\"pagination\">" +
                            "<li><a aria-label=\"Previous\" href=\"Page2\"><span aria-hidden=\"true\">&laquo;</span></a></li>" +
                            "<li><a href=\"Page1\">1</a></li><li><a href=\"Page2\">2</a></li>" +
                            "<li class=\"active\"><a href=\"Page3\">3<span class=\"sr-only\">(current)</span></a></li>" +
                            "<li class=\"disabled\"><a aria-label=\"Next\"><span aria-hidden=\"true\">&raquo;</span></a></li>" +
                            "</ul></nav>", result.ToString());
        }

        [TestMethod]
        public void Can_Send_Pagination_View_Model()
        {
            // Организация (arrange)
            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games).Returns(new List<Game>
            {
                new Game {GameId = 1, Name = "Игра1"},
                new Game {GameId = 2, Name = "Игра2"},
                new Game {GameId = 3, Name = "Игра3"},
                new Game {GameId = 4, Name = "Игра4"},
                new Game {GameId = 5, Name = "Игра5"}
            });

            GameController controller = new GameController(mock.Object)
            {
                pageSize = 3
            };

            // Action
            GamesListViewModel result = (GamesListViewModel) controller.List(null, 2).Model;

            //Assert 
            PagingInfo pageInfo = result.PagingInfo;
            Assert.AreEqual(pageInfo.CurrentPage, 2);
            Assert.AreEqual(pageInfo.ItemsPerPage, 3);
            Assert.AreEqual(pageInfo.TotalItems, 5);
            Assert.AreEqual(pageInfo.TotalPages, 2);


        }

        [TestMethod]
        public void Can_Filter_Games()
        {
            //Организация (arrange)
            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games).Returns(new List<Game>()
            {
                new Game {GameId = 1, Name = "Игра1", Category = "Cat1"},
                new Game {GameId = 2, Name = "Игра2", Category = "Cat2"},
                new Game {GameId = 3, Name = "Игра3", Category = "Cat1"},
                new Game {GameId = 4, Name = "Игра4", Category = "Cat2"},
                new Game {GameId = 5, Name = "Игра5", Category = "Cat3"}
            });

            GameController controller = new GameController(mock.Object)
            {
                pageSize = 3
            };

            //Action
            var model = (GamesListViewModel) controller.List("Cat2", 1).Model;
            var result = model.Games.ToList();

            //Assert
            Assert.AreEqual(result.Count(), 2);
            Assert.IsTrue(result[0].Name == "Игра2" && result[0].Category == "Cat2");
            Assert.IsTrue(result[1].Name == "Игра4" && result[0].Category == "Cat2");



        }

        [TestMethod]
        public void Can_Create_Categories()
        {
            //Организация (arrange)
            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games).Returns(new List<Game>()
            {
                new Game {GameId = 1, Name = "Игра1", Category = "Cat5"},
                new Game {GameId = 2, Name = "Игра2", Category = "Cat2"},
                new Game {GameId = 3, Name = "Игра3", Category = "Cat5"},
                new Game {GameId = 4, Name = "Игра4", Category = "Cat2"},
                new Game {GameId = 5, Name = "Игра5", Category = "Cat3"}
            });

            // Организация - создание контроллера
            NavController controller = new NavController(mock.Object);

            // Действие - получение набора категорий
            var result = (IEnumerable<IGrouping<string, Game>>) controller.Menu().Model;
            var res = result.Select(item => new {category = item.Key, count = item.Count() }).ToList();

            //Assert
            Assert.AreEqual(res.Count(), 3);
            Assert.IsTrue(res[0].category == "Cat2" && res[0].count == 2);
            Assert.IsTrue(res[1].category == "Cat3" && res[1].count == 1);
            Assert.IsTrue(res[2].category == "Cat5" && res[2].count == 2);
        }

        [TestMethod]
        public void Indicates_Selected_Category()
        {
            // Организация - создание имитированного хранилища
            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games).Returns(new Game[]
            {
                new Game {GameId = 1, Name = "Game1", Category = "Simulator"},
                new Game {GameId = 2, Name = "Game2", Category = "Shuter"}
            });

            // Организация - создание контроллера
            NavController controller = new NavController(mock.Object);

            // Организация - определение выбранной категории
            string categoryToSelect = "Shuter";

            // Действие
            string result = controller.Menu(categoryToSelect).ViewBag.SelectedCategory;

            // Утверждение
            Assert.AreEqual(categoryToSelect, result);
        }

        [TestMethod]
        public void Generate_Category_Specific_Game_Count()
        {
            // Организация (arrange)
            Mock<IGameRepository> mock = new Mock<IGameRepository>();
            mock.Setup(m => m.Games).Returns(new List<Game>()
            {
                new Game {GameId = 1, Name = "Игра1", Category = "Cat1"},
                new Game {GameId = 2, Name = "Игра2", Category = "Cat2"},
                new Game {GameId = 3, Name = "Игра3", Category = "Cat1"},
                new Game {GameId = 4, Name = "Игра4", Category = "Cat2"},
                new Game {GameId = 5, Name = "Игра5", Category = "Cat3"}
            });

            // Организация - создание контроллера
            GameController controller = new GameController(mock.Object);
            controller.pageSize = 3;

            // Действие - тестирование счетчиков товаров для различных категорий
            int res1 = ((GamesListViewModel) controller.List("Cat1").Model).PagingInfo.TotalItems;
            int res2 = ((GamesListViewModel) controller.List("Cat2").Model).PagingInfo.TotalItems;
            int res3 = ((GamesListViewModel) controller.List("Cat3").Model).PagingInfo.TotalItems;
            int resAll = ((GamesListViewModel) controller.List(null).Model).PagingInfo.TotalItems;

            // Утверждение
            Assert.AreEqual(res1, 2);
            Assert.AreEqual(res2, 2);
            Assert.AreEqual(res3, 1);
            Assert.AreEqual(resAll, 5);


        }

    }
}
