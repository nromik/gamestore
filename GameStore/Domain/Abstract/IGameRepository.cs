﻿
using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Abstract
{
    public interface IGameRepository
    {
            IEnumerable<Game> Games { get; }
    }
}