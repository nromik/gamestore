﻿using System.Collections.Generic;
using Domain.Entities;
using Domain.Abstract;

namespace Domain.Concrete
{
    public class EFGameRepository : IGameRepository
    {

        EFDbContext context = new EFDbContext();
        public IEnumerable<Game> Games {
            get { return context.Games; }
        }

        
    }
}