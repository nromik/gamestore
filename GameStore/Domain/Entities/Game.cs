﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Game
    {
        [Key]
        public int GameId { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [MaxLengthAttribute]
        public string Description { get; set; }

        [StringLength(255)]
        public string Category { get; set; }

        public decimal Price { get; set; }
    }
}