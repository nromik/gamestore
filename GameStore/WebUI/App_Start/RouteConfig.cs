﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "",
                defaults: new {controller = "Game", action = "List", category = (string) null, page = 1}
                );

            routes.MapRoute(
                name: "Page",
                url: "Page-{page}",
                defaults: new {controller = "Game", action = "List", category = (string) null},
                constraints: new {page = @"\d+"}

                );

            routes.MapRoute(
                name: "Category",
                url: "Category-{category}",
                defaults: new { controller = "Game", action = "List", page = 1}
                );
            routes.MapRoute(
                name: "Category+Page",
                url: "Category-{category}/Page-{page}",
                defaults: new {controller = "Game", action = "List"},
                constraints: new {page = @"\d+"}
                );

            routes.MapRoute(
                name: "Controler+action",
                url: "{controller}/{action}"
                );

        }
    }
}
