﻿using System;

namespace WebUI.Models
{
    public class PagingInfo
    {

        /// <summary>
        /// Кол-во товаров
        /// </summary>
        public int TotalItems { get; set; }

        /// Кол-во товаров на одной странице
        public int ItemsPerPage { get; set; }

        /// Номер текущей страницы
        public int CurrentPage { get; set; }

        /// Общее кол-во страниц
        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage); }
        }


    }
}