﻿using System;
using System.Text;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.HtmlHelpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html,
            PagingInfo pagingInfo, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();

            /*
            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());
            }
            */

            result.Append("<nav aria-label=\"Page navigation\">" +
                            "<ul class=\"pagination\">");
            {
                TagBuilder tagLiPrevious = new TagBuilder("li");

                if(pagingInfo.CurrentPage == 1)
                    tagLiPrevious.AddCssClass("disabled");

                TagBuilder tagAPrevious = new TagBuilder("a");

                if (pagingInfo.CurrentPage != 1)
                    tagAPrevious.MergeAttribute("href", pageUrl(pagingInfo.CurrentPage-1));
               
                tagAPrevious.MergeAttribute("aria-label", "Previous");
                tagAPrevious.InnerHtml = "<span aria-hidden=\"true\">&laquo;</span>";

                tagLiPrevious.InnerHtml = tagAPrevious.ToString();
                result.Append(tagLiPrevious);
            }

            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                var tagLi = new TagBuilder("li");

                var tagA = new TagBuilder("a");
                tagA.MergeAttribute("href", pageUrl(i));
                tagA.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                {
                    tagLi.AddCssClass("active");
                    tagA.InnerHtml += "<span class=\"sr-only\">(current)</span>";
                }
                tagLi.InnerHtml = tagA.ToString();

                result.Append(tagLi);
            }
            {
                TagBuilder tagLiNext = new TagBuilder("li");

                if(pagingInfo.CurrentPage == pagingInfo.TotalPages)
                    tagLiNext.AddCssClass("disabled");

                TagBuilder tagANext = new TagBuilder("a");

                if (pagingInfo.CurrentPage != pagingInfo.TotalPages)
                    tagANext.MergeAttribute("href", pageUrl(pagingInfo.CurrentPage + 1));

                tagANext.MergeAttribute("aria-label", "Next");
                tagANext.InnerHtml = "<span aria-hidden=\"true\">&raquo;</span>";

                tagLiNext.InnerHtml = tagANext.ToString();
                result.Append(tagLiNext);
            }

            result.Append("</ul>" +
                          "</nav>");

            return MvcHtmlString.Create(result.ToString());



        }
    }
}