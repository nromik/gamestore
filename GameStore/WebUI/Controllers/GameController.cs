﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Abstract;
using WebUI.Models;



namespace WebUI.Controllers
{
    public class GameController : Controller
    {

        private IGameRepository repository;
        public int pageSize = 4;


        public GameController(IGameRepository repo)
        {
            repository = repo;
        }


        public ViewResult List(string category, int page = 1)
        {
            //return View(repository.Games);

            /*
            var result = repository.Games
                .OrderBy(game => game.GameId)
                .Skip((page - 1)*pageSize)
                .Take(pageSize);
            return View(result);
            */
            GamesListViewModel model = new GamesListViewModel()
            {
                Games = repository.Games
                    .Where(p => category == null || p.Category == category)
                    .OrderBy(game => game.GameId)
                    .Skip((page - 1)*pageSize)
                    .Take(pageSize),
                PagingInfo = new PagingInfo()
                {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    TotalItems = repository.Games
                    .Count(p => category == null || p.Category == category)
                },
                CurrentCategory = category
                
            };
            if (model.Games.Count() == 0)
            {
                ViewBag.ErrorMessage = "model.Games == null";
                return View("Error");
            }

            return View(model);

        }

    }




   
}