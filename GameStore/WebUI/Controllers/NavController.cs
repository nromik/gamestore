﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Abstract;
using Domain.Entities;

namespace WebUI.Controllers
{
    public class NavController : Controller
    {
        private IGameRepository repository;

        public NavController(IGameRepository repo)
        {
            this.repository = repo;
        }

        public PartialViewResult Menu(string category = null)
        {
            ViewBag.SelectedCategory = category;

            /*
            IEnumerable<string> categories = repository.Games
                .Select(game => game.Category)
                .Distinct()
                .OrderBy(x => x);
            */

            ViewBag.CountGames = repository.Games.Count();

            IEnumerable<IGrouping<string, Game>> categoriesGroup = repository.Games
                .GroupBy(game => game.Category)
                .OrderBy(x => x.Key);
           
            return PartialView(categoriesGroup);

        }
    }
}